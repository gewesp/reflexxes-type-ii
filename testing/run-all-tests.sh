#! /bin/bash

# Run all tests.
# After running, use git diff to compare test output with repo version and
# explain or address any discrepancies.
#
# TODO: More test coverage
# TODO: Test release and debug versions
# TODO: Clean cross-platform support.
#       The code is actually enough cross-platform such that we don't need
#       to distinguish between Linux and MacOS.
if [ Darwin = `uname` -o Linux = `uname` ] ; then
  PLATFORM=MacOS
  # Are there any other MacOS architectures besides x64 as of 2017?
  ARCH=x64
else
  echo "Error: Platform `uname` is not yet supported"
  echo "Please feel free to add and file a pull request"
  exit 1
fi

CONFIGURATION=debug

BINARY_DIR=$PLATFORM/$ARCH/$CONFIGURATION/bin
PLOT=plotting/plot.py

echo Testing binaries in $BINARY_DIR ...

INPUT=testing/input
GOLDEN_OUTPUT=testing/golden
BINARIES=`ls $BINARY_DIR/0[1-9]*`

for bin in $BINARIES ; do
  b=`basename $bin`
  echo "Executing $BINARY_DIR/$b > $GOLDEN_OUTPUT/$b.txt"
  $BINARY_DIR/$b > $GOLDEN_OUTPUT/$b.txt
done

function csv_test() {
  csv_file="$GOLDEN_OUTPUT/$1.csv"
  cmd="$BINARY_DIR/17_RMLJSONExample $INPUT/$1.json $csv_file"
  echo Executing $cmd
  $cmd > $GOLDEN_OUTPUT/$1.txt
  $PLOT --pdf "$csv_file"
}


# Anything starting not with 0x_... has input files
echo Executing "$BINARY_DIR/17_RMLJSONExample $INPUT/17-default.json > $GOLDEN_OUTPUT/17-default.txt"
$BINARY_DIR/17_RMLJSONExample $INPUT/17-default.json > $GOLDEN_OUTPUT/17-default.txt

# Tests with CSV output
csv_test 17-csv1
csv_test 17-phase
csv_test 17-2j-vboundary-0
csv_test 17-smooth
csv_test 17-1j-smooth-demo
csv_test 17-1j-nosmooth-demo
