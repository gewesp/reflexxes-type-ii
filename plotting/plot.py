#! /usr/bin/python

#
# Tool to plot position, velocity and accelerations in joint space.
#
# Usage: plot.py [filename.csv ...]
#
# Input: .csv file(s) with lines in the following format:
#   t, p_1, ..., p_n, v_1, ..., v_n, a_1, ..., a_n
# p ... position
# v ... velocity
# a ... acceleration
#
# Requires: matplotlib, numpy
#
# TODO: 
# * Legends
# * Export to PDF etc.
#

import argparse
import os
import sys

parser = argparse.ArgumentParser(description='Plot trajectory files')
parser.add_argument('--pdf', help='PDF output only (non-interactive)',
                    action='store_true')
# 'metavar' is actually the string to display in help text.  WTF.
parser.add_argument('files', metavar='files', type=str, nargs='*',
                    help='List of CSV files to plot')
args = parser.parse_args()


import matplotlib

if args.pdf:
  # Must come directly after matplotlib import.  WTF.
  matplotlib.use('Agg')

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np


def plot(data, pdfname = 'out.pdf'):
  """
  data is the above input format transposed:

  data[0]: time
  data[1, ..., n_joints]: Position values
  data[n_joints + 1, ..., 2n_joints]: Velocity values
  data[2n_joints + 1, ..., 3n_joints]: Acceleration values
  """
  n_rows   = len(data   )
  n_colums = len(data[0])
  # First column: time; 3 values pos, vel, acc per joint.
  n_joints = (n_rows - 1) / 3

  t = data[0]

  pages = PdfPages(pdfname)

  for j in xrange(n_joints):
    pos = data[(               1 + j), :]
    vel = data[(    n_joints + 1 + j), :]
    acc = data[(2 * n_joints + 1 + j), :]
    
    fig = plt.figure(j + 1)

    plt.plot(t, pos, t, vel, t, acc)
    plt.draw()
    plt.savefig(pages, format='pdf')
 
  pages.close()
  plt.show()


for fname in args.files:
  data = np.genfromtxt(fname, delimiter=',')
  pdfname = 'pdf/' + os.path.basename(os.path.splitext(fname)[0]) + '.pdf'
  plot(data.transpose(), pdfname)
