//  ---------------------- Doxygen info ----------------------
//! \file 17_RMLJSONExample.cpp
//!
//! \brief
//! Test application number 2 for the Reflexxes Motion Libraries
//! (position-based interface, complete description of output values)
//!
//! Reads initial, final pose and velocity limits from a JSON file,
//! generates a trajectory and outputs the result on stdout.
//!
//! TODO: 
//! * Document JSON format and units.  Unfortunately, boost::property_tree
//!   doesn't allow comments in the JSON file.
//! * Improve output format; factor out vector output loops to separate
//!   functions
//!
//! \date March 2017
//!
//! \version 1.2.6 (Reflexxes)
//!
//! \author Torsten Kroeger, <info@reflexxes.com> and Gerhard Wesp <kisstech.ch@gmail.com> \n
//!
//! \copyright Copyright (C) 2014 Google, Inc.
//! \copyright Copyright (C) 2017 KISS Technologies GmbH
//! \n
//! \n
//! <b>GNU Lesser General Public License</b>
//! \n
//! \n
//! This file is part of the Type II Reflexxes Motion Library.
//! \n\n
//! The Type II Reflexxes Motion Library is free software: you can redistribute
//! it and/or modify it under the terms of the GNU Lesser General Public License
//! as published by the Free Software Foundation, either version 3 of the
//! License, or (at your option) any later version.
//! \n\n
//! The Type II Reflexxes Motion Library is distributed in the hope that it
//! will be useful, but WITHOUT ANY WARRANTY; without even the implied
//! warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
//! the GNU Lesser General Public License for more details.
//! \n\n
//! You should have received a copy of the GNU Lesser General Public License
//! along with the Type II Reflexxes Motion Library. If not, see
//! <http://www.gnu.org/licenses/>.
//  ----------------------------------------------------------
//   For a convenient reading of this file's source code,
//   please use a tab width of four characters.
//  ----------------------------------------------------------

#include "util.h"

#include <stdio.h>
#include <stdlib.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <cmath>

#include <ReflexxesAPI.h>
#include <RMLPositionFlags.h>
#include <RMLPositionInputParameters.h>
#include <RMLPositionOutputParameters.h>

#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"


//*************************************************************************
// boost::property_tree helpers
// TODO: Factor out to a separate file/library
//*************************************************************************

// Reads 'key' and tries to interpret it as a vector<T>, e.g.
// vector<double>.
template <typename T>
std::vector<T> as_vector(
    boost::property_tree::ptree const& pt,
    std::string const& key) {
    std::vector<T> r;
    for (auto& item : pt.get_child(key))
        r.push_back(item.second.get_value<T>());
    return r;
}

// Reads a double vector from a property_tree
// and verifies it has n_elements elements.
std::vector<double> get_double_vector(
    const boost::property_tree::ptree& pt,
    const std::string& key,
    const int n_elements) {
  try {
    const auto ret = as_vector<double>(pt, key);
    if (n_elements != ret.size()) {
      throw std::runtime_error(
          "should have " + std::to_string(n_elements)
          + " element(s) (found "
          + std::to_string(ret.size())
          + ")");
    }
    return ret;
  } catch (const std::exception& e) {
    throw std::runtime_error("key " + key + ": " + e.what());
  }
}

// Gets a vector<double> at 'key' in 'pt', copies it to an array
// of at least n_elements.
void set_array(
    const boost::property_tree::ptree& pt,
    const std::string& key,
    const int n_elements,
    double* const dest) {
  const auto v = get_double_vector(pt, key, n_elements);
  std::copy(v.begin(), v.end(), dest);
}

//*************************************************************************
// Other helper functions
//*************************************************************************

void write_vector(FILE* const out, double* const v, const int n) {
  for ( int j = 0; j < n; j++)
  {
      fprintf(out, "%10.3lf ", v[j]);
  }
  fprintf(out, "\n");
}

// Write n comma-separated values, no linefeed, no comma after
// last value.
template<typename T>
void write_csv(std::ostream& os, T const* const v, const int n) {
  for (int i = 0; i < n; ++i) {
    os << v[i];
    if (i + 1 < n) {
      os << ',';
    }
  }
}

//*************************************************************************
// Smoothing by convolution
//*************************************************************************

// Creates a kernel based on parameters from pt.
// dt: Time delta used to determine kernel width in samples
std::vector<double> make_kernel(
    boost::property_tree::ptree const& pt,
    const double& dt) {
  const auto type = pt.get<std::string>("kernel_type");

  const double width = pt.get<double>("kernel_width");
  if (width <= 0) {
    throw std::runtime_error("rectangular kernel width must be > 0");
  }

  const long n = std::ceil(width / dt);

  if        ("rectangular" == type) {
    return util::rectangular_kernel(n);
  } else if ("cosine"      == type) {
    return util::cosine_kernel(n);
  } else {
    throw std::runtime_error("unknown kernel type: " + type);
  }
}


//*************************************************************************
// This function contains source code to get started with the Type II
// Reflexxes Motion Library. Based on the program
// 01_RMLPositionSampleApplication.cpp, this sample code becomes extended by
// using (and describing) all available output values of the algorithm.
// As in the former example, we compute a trajectory for a system with
// three degrees of freedom starting from an arbitrary state of motion.
// This code snippet again directly corresponds to the example trajectories
// shown in the documentation.
//*************************************************************************
void type_ii(
    const boost::property_tree::ptree& pt, 
    const std::string& csv_filename = "") {

    std::ofstream output_file;
    if ("" != csv_filename) {
      output_file.open(csv_filename);
    }
    const double dt       = std::stod(pt.get<std::string>("dt"      ));
    const int    n_joints = std::stod(pt.get<std::string>("n_joints"));
    std::string synchronization = "time";
    if (pt.count("synchronization") > 0) {
      synchronization = pt.get<std::string>("synchronization");
    }

    // ********************************************************************
    // Creating all relevant objects of the Type II Reflexxes Motion Library

    ReflexxesAPI RML(n_joints, dt);
    RMLPositionInputParameters  IP(n_joints);
    RMLPositionOutputParameters OP(n_joints);

    // ********************************************************************
    // Set-up a timer with a period of one millisecond
    // (not implemented in this example in order to keep it simple)
    // ********************************************************************

    printf("-------------------------------------------------------\n"  );
    printf("Reflexxes Motion Libraries                             \n"  );
    printf("Example: 17_JSONExample.cpp                            \n\n");
    printf("This example demonstrates the use of the entire output \n"  );
    printf("values of the position-based Online Trajectory         \n"  );
    printf("Generation algorithm of the Type II Reflexxes Motion   \n"  );
    printf("Library.                                               \n\n");
    printf("Copyright (C) 2014 Google, Inc.                        \n"  );
    printf("Copyright (C) 2017 KISS Technologies, GmbH             \n"  );
    printf("-------------------------------------------------------\n"  );

    // ********************************************************************
    // Set-up the input parameters

    // In this test program, arbitrary values are chosen. If executed on a
    // real robot or mechanical system, the position is read and stored in
    // an RMLPositionInputParameters::CurrentPositionVector vector object.
    // For the very first motion after starting the controller, velocities
    // and acceleration are commonly set to zero. The desired target state
    // of motion and the motion constraints depend on the robot and the
    // current task/application.
    // The internal data structures make use of native C data types
    // (e.g., IP.CurrentPositionVector->VecData is a pointer to
    // an array of n_joints double values), such that the Reflexxes
    // Library can be used in a universal way.

    set_array(pt, "initial_position"     , n_joints , IP.CurrentPositionVector    ->VecData);
    set_array(pt, "initial_velocity"     , n_joints , IP.CurrentVelocityVector    ->VecData);
    set_array(pt, "initial_acceleration" , n_joints , IP.CurrentAccelerationVector->VecData);
 
    set_array(pt, "max_velocity"         , n_joints , IP.MaxVelocityVector        ->VecData);
    set_array(pt, "max_acceleration"     , n_joints , IP.MaxAccelerationVector    ->VecData);
    set_array(pt, "max_jerk"             , n_joints , IP.MaxJerkVector            ->VecData);
 
    set_array(pt, "target_position"      , n_joints , IP.TargetPositionVector     ->VecData);
    set_array(pt, "target_velocity"      , n_joints , IP.TargetVelocityVector     ->VecData);

    IP.MinimumSynchronizationTime =
        std::stod(pt.get<std::string>("minimum_synchronization_time"));

    std::fill(IP.SelectionVector->VecData            ,
              IP.SelectionVector->VecData + n_joints ,
              true);

    if (!IP.CheckForValidity())
    {
        throw std::runtime_error("Input values are INVALID!\n");
    }

    // ********************************************************************
    // Variables needed for the control loop

    // TODO: Comment on which flags we're using?  Defaults?
    RMLPositionFlags Flags;

    if ("time" == synchronization) {
      Flags.SynchronizationBehavior =
          RMLPositionFlags::ONLY_TIME_SYNCHRONIZATION;
    } else if ("phase_if_possible" == synchronization) {
      Flags.SynchronizationBehavior =
          RMLPositionFlags::PHASE_SYNCHRONIZATION_IF_POSSIBLE;
    } else {
      throw std::runtime_error("unknown synchronization parameter: " + synchronization);
    }

    bool FirstCycleCompleted = false;
    int ResultValue = 0;
    long LoopCount = 0;

    // ********************************************************************
    // Starting the control loop

    const int Rows = 1 + 3 * n_joints;
    // 1 + 3 * n_joints vectors of time series.
    std::vector<std::vector<double>> Trajectory(Rows);

    while (ResultValue != ReflexxesAPI::RML_FINAL_STATE_REACHED)
    {

        // ****************************************************************
        // Wait for the next timer tick
        // (not implemented in this example in order to keep it simple)
        // ****************************************************************

        // Calling the Reflexxes OTG algorithm
        ResultValue =   RML.RMLPosition(       IP
                                           ,  &OP
                                           ,   Flags       );

        ++LoopCount;

        if (ResultValue < 0)
        {
            printf("An error occurred (%d).\n", ResultValue );
            break;
        }

        // ****************************************************************
        // The following part completely describes all output values
        // of the Reflexxes Type II Online Trajectory Generation
        // algorithm.

        if (!FirstCycleCompleted)
        {
            FirstCycleCompleted =   true;

            printf("-------------------------------------------------------\n");
            printf("General information:\n\n");
            if (output_file.is_open()) {
                printf("CSV data: Writing to %s\n", csv_filename.c_str());
            } else {
                printf("CSV data: Not writing\n");
            }

            printf("The execution time of the current trajectory is %.3lf seconds.\n", OP.GetSynchronizationTime());

            if (OP.IsTrajectoryPhaseSynchronized())
            {
                printf("The current trajectory is phase-synchronized.\n");
            }
            else
            {
                printf("The current trajectory is time-synchronized.\n");
            }
            if (OP.WasACompleteComputationPerformedDuringTheLastCycle())
            {
                printf("The trajectory was computed during the last computation cycle.\n");
            }
            else
            {
                printf("The input values did not change, and a new computation of the trajectory parameters was not required.\n");
            }

            printf("-------------------------------------------------------\n");
            printf("New state of motion:\n\n");

            printf("New position/pose vector                  : ");
            write_vector(stdout, OP.NewPositionVector->VecData, n_joints);
            printf("New velocity vector                       : ");
            write_vector(stdout, OP.NewVelocityVector->VecData, n_joints);
            printf("New acceleration vector                   : ");
            write_vector(stdout, OP.NewAccelerationVector->VecData, n_joints);
            printf("-------------------------------------------------------\n");
            printf("Extremes of the current trajectory:\n");

            for ( int i = 0; i < n_joints; i++)
            {
                printf("\n");
                printf("Degree of freedom                         : %d\n", i);
                printf("Minimum position                          : %10.3lf\n", OP.MinPosExtremaPositionVectorOnly->VecData[i]);
                printf("Time, at which the minimum will be reached: %10.3lf\n", OP.MinExtremaTimesVector->VecData[i]);
                printf("Position/pose vector at this time         : ");
                write_vector(stdout, OP.MinPosExtremaPositionVectorArray[i]->VecData, n_joints);
                printf("Velocity vector at this time              : ");
                write_vector(stdout, OP.MinPosExtremaVelocityVectorArray[i]->VecData, n_joints);
                printf("Acceleration vector at this time          : ");
                write_vector(stdout, OP.MinPosExtremaAccelerationVectorArray[i]->VecData, n_joints);
                printf("Maximum position                          : %10.3lf\n", OP.MaxPosExtremaPositionVectorOnly->VecData[i]);
                printf("Time, at which the maximum will be reached: %10.3lf\n", OP.MaxExtremaTimesVector->VecData[i]);
                printf("Position/pose vector at this time         : ");
                write_vector(stdout, OP.MaxPosExtremaPositionVectorArray[i]->VecData, n_joints);
                printf("Velocity vector at this time              : ");
                write_vector(stdout, OP.MaxPosExtremaVelocityVectorArray[i]->VecData, n_joints);
                printf("Acceleration vector at this time          : ");
                write_vector(stdout, OP.MaxPosExtremaAccelerationVectorArray[i]->VecData, n_joints);
            }
            printf("-------------------------------------------------------\n");
        }
        // ****************************************************************

        // ****************************************************************
        // Feed the output values of the current control cycle back to
        // input values of the next control cycle

        // TODO: Are VecData pointer members?!  Guarded against leaks?
        // TODO: Didn't find a 'current time' value in the API.  It sure must
        // exist?!
        Trajectory.at(0).push_back(dt * (LoopCount - 1));
        for (int i = 0; i < n_joints; ++i) {
          Trajectory.at(1                + i).push_back(
              IP.CurrentPositionVector->VecData[i]);
          Trajectory.at(1 +     n_joints + i).push_back(
              IP.CurrentVelocityVector->VecData[i]);
          Trajectory.at(1 + 2 * n_joints + i).push_back(
              IP.CurrentAccelerationVector->VecData[i]);
        }

        *IP.CurrentPositionVector      =   *OP.NewPositionVector      ;
        *IP.CurrentVelocityVector      =   *OP.NewVelocityVector      ;
        *IP.CurrentAccelerationVector  =   *OP.NewAccelerationVector  ;
    }

    printf("Loop count                                :    %ld\n", LoopCount);

    const double t_orig = dt * LoopCount;

    if (pt.count("smooth") > 0) {
      const auto kernel = make_kernel(pt.get_child("smooth"), dt);
      const long n = kernel.size() - 1;
      printf("Smoothing trajectory, kernel size: %ld\n", n);

      // Convolve pos, vel, acc
      for (int i = 1; i < Rows; ++i) {
        Trajectory.at(i) = util::convolve_slow(Trajectory.at(i), kernel);
      }

      // Extend time to the left and right
      auto& t = Trajectory.at(0);
      const long n_front =     n / 2;
      const long n_back  = n - n / 2;
      assert(n_front + n_back == n);
      
      t.insert(t.begin(), n_front, 0.0);
      t.insert(t.end  (), n_back , 0.0);
      for (long i = 0; i < n_front; ++i) {
        t.at(i) = dt * (i - n_front);
      }
      for (long i = 0; i < n_back; ++i) {
        t.at(t.size() - n_back + i) = t_orig + dt * i;
      }
    }

    // Verify vector sizes
    for (int i = 1; i < Rows; ++i) {
      assert(Trajectory.at(0).size() == Trajectory.at(i).size());
    }

    // Transpose and write to file
    if (output_file.is_open()) {
      for (unsigned long i = 0; i < Trajectory.at(0).size(); ++i) {
        std::vector<double> state;
        for (unsigned j = 0; j < Rows; ++j) {
          state.push_back(Trajectory.at(j).at(i));
        }
        write_csv(output_file, &state[0], Rows);
        output_file << '\n';
      }
    }
}

void usage(std::ostream& os, const std::string& prog) {
  os << "Usage: " << prog << " <filename>.json [outputfile.csv]"
     << std::endl;
}


int main(const int argc, const char* const* const argv) {
  try {
    if (argc < 2 || argc > 3) {
      usage(std::cout, argv[0]);
      return 1;
    }

    const std::string filename = argc >= 3 ? argv[2] : "";

    boost::property_tree::ptree pt;
    boost::property_tree::read_json(argv[1], pt);

    type_ii(pt, filename);
  } catch (const std::exception& e) {
    std::cerr << "Error: " << e.what() << std::endl;
    usage(std::cerr, argv[0]);
    return 1;
  }
}
