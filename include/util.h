#ifndef UTIL_H
#define UTIL_H

#include <exception>
#include <stdexcept>
#include <vector>

#include <cassert>
#include <cmath>

namespace util {

constexpr double pi = 3.141592653589793238462643;

//
// Returns convolution f * k
//
// k is supposed to be a kernel, i.e. symmetric.
//
// Slow implementation, O(f.size() * k.size()).  Only use
// for testing and/or small kernels.
//
// Prior to convolving, extends f to the left and right by k.size() - 1
// values f.front() and f.back(), resp.
//

template<typename T>
std::vector<T> 
convolve_slow(std::vector<T> const& f_in, std::vector<T> const& k) {
  if (f_in.empty() || k.empty()) {
    throw std::runtime_error(
        "convolve_slow(): at least one empty input vector");
  }

  // Input sizes
  long const nf = f_in.size();
  long const nk = k   .size();
  // Output size
  long const n  = nf + nk - 1;

  // Extend f_in to left and right
  auto f = f_in;
  f.insert(f.begin(), nk, f_in.front());
  f.insert(f.end  (), nk, f_in.back ());

  std::vector<T> out(n, T());
  for (long i = 0; i < n; ++i) {
    for(auto j = 0; j < nk; ++j) {
      assert(    i < out.size());
      assert(i + j < f  .size());
      assert(     nk - 1 - j < k.size());
      assert(0 <= nk - 1 - j           );
      out.at(i) += f.at(i + j) * k.at(nk - 1 - j);
    }
  }
  return out; 
}

// Returns a rectangular kernel of n samples 
inline std::vector<double> rectangular_kernel(const long n) {
  if (n < 1) {
    throw std::runtime_error("kernel samples must be >= 1");
  }

  return std::vector<double>(n, 1.0 / n);
}

// Normalized kernel with a bell shape from a cosine over [-pi, pi].
inline std::vector<double> cosine_kernel(const long n) {
  if (n < 1) {
    throw std::runtime_error("kernel samples must be >= 1");
  }

  // f(0) == f(n-1) == 0, this is still singular for n == 2
  if (n <= 2) {
    return rectangular_kernel(1);
  }

  assert(2 <= n);
  const auto f = [n](const long i) {
    return 1 + std::cos(2 * i * pi / (n - 1) - pi);
  };

  double sum = 0;
  for (int i = 0; i <= n - 1; ++i) {
    sum += f(i);
  }

  assert(sum > 0);

  std::vector<double> k(n);
  for (int i = 0; i <= n - 1; ++i) {
    k.at(i) = f(i) / sum;
  }

  return k;
}

} // end namespace util

#endif // UTIL_H
